---
title:  Chapitre 11  algorithmes de tri
---


Cours de Frédéric Junier



## Cours 

* [Cours/TP version pdf](chapitre11/1NSI-Cours-Tris-2021V1.pdf)
* [Matériel élève (squelette Python)](chapitre11/materiel.zip)
* [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
* [Cours/TP correction sur Capytale](https://capytale2.ac-paris.fr/web/c/29c9-255106)
* [Cours/TP correction version pdf](chapitre11/ressources/Cours_Tris_Premiere_NSI_2021_Correction.pdf)

## Visualisation

Un bon outil en ligne :  <http://fred.boissac.free.fr/AnimsJS/Dariush_Tris/index.html>
## Tutoriels vidéo 


??? video

    [Algorithmes de tri par Pierre Marquestaut](https://peertube.lyceeconnecte.fr/videos/watch/cffa5c51-e0fa-4ef7-9437-743a683fc937).

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.lyceeconnecte.fr/videos/embed/cffa5c51-e0fa-4ef7-9437-743a683fc937" frameborder="0" allowfullscreen></iframe>
