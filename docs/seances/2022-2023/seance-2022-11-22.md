---
title: Séance du 22/11/2022
---

# Séance du 22/11/2022


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.


## Automatismes

Travail sur les tableaux à une dimension :

* Recherche séquentielle : 
    * [Liste des index des occurrennces](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=533)
    * [Valeur et indice du maximum](https://e-nsi.gitlab.io/pratique/N1/200-val_ind_max/sujet/)
* Parcours de tableau :
    * [Tableau décroissant](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=450)
    * [Tous différents](https://e-nsi.gitlab.io/pratique/N1/420-tous_differents/sujet/)

##  Chapitre 5 : tableaux à deux dimensions / Traitement d'images

* [Cours / TP en version pdf](../chapitre7/NSI-Images-Tableaux2d--2021V2.pdf)
  
* [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)

* [Matériel élèves (pour travailler hors connexion)](../chapitre7/Images-Tableaux2d-materiel.zip)

## Travail à faire


* Pour jeudi 24/11 : 
    * [QCM sur les tableaux en compréhension](https://genumsi.inria.fr/qcm.php?h=66f6a8ff3099844f78f411bd489183ba) et [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTE4OzExOTsxNDE7MTQ3OzIxNzsyNjY7Mjc2OzMwMjszMDY7MzIzOzMyNTszMjY7NDMxOzQzMjs0MzM7NDM0OzQzNTs0MzY7NDM3OzQ0MDsxMzc2)
    * [QCM sur les tableaux à deux dimensions](https://genumsi.inria.fr/qcm.php?h=24a7b78df2ca32192ec7ef53f5d7b6b5) et [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTUwOzI3MjsyNzM7Mjc0OzMxMjszMzk7Mzk5OzQyNjs0Njc7NDcxOzEzMzA7MTM3NjsxNDI4OzE0Mjk=)
* Pour le samedi 26/11 : rendu de l'étape 2 du projet lapins crétins, code à déposer sur [espace de dépôt Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1458&forceview=1)