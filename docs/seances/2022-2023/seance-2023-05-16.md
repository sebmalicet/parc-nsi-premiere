---
title: Séance du 11/05/2023
---

# Séance du 16/05/2023

## Retour du DS 9

## IHM sur le Web, protocole HTTP et formulaires (60 minutes)

Travail en autonomie, les élèves s'approprient tout seul le cours à partir des différents exemples et exercices. Il vérifient leurs réponses à l'aide de la correction. Travail dans le navigateur Firefox avec les outils de développement.
Des points étapes réguliers sont prévus pour une restitution collective.

- [Lien vers le chapitre 21 avec le cours](../chapitre21.md)
- **Premier point étape (10 minutes de travail individuel)** après l'exercice 3
- **Second point étape (15 minutes de travail individuel)** après l'exemple 1 => **Point de cours 2** sur les _Méthodes de passage des paramètres : GET ou POST_ puis **Exercice 4** en collectif.
- **Second point étape (30 minutes de travail individuel)** après le **Point de cours 3** sur les _éléments de formulaire HTML_ et l'exercice 5 => Synthèse en commun.

## Travail sur le projet de fin d'année

- [Tutoriel pyxel](../Projets/Pyxel/decouverte_pyxel.md)
- [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)
- [Inscription pour le projet final](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
- [Fiche de suivi (pdf modifiable)](../Projets/Projets2023/ProjetFinal/Cadrage/Fiche_Suivi_Projet_Final_2023.pdf) à remplir individuellement et déposer au plus tard le mardi 25 avril dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1841>
- Projet (code + capsule audio de présentation individuelle de 5 minutes) à déposer au plus tard le dimanche 21 mai dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843>

## Travail à faire

!!! task

    * Projet Final : Déposez au plus tard le dimanche 21 mai dans <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843> :
        * une archive au format zip avec votre code et toutes les ressources nécessaires
        * une capsule audio ou video individuelle de 5 minutes présentant le projet avec un focus sur une de vos contributions au projet : plan structuré avec introduction et conclusion
