---
title: Séance du 15/09/2022
---

# Séance du 15/09/2022


#
## Chapitre 1: constructions de bases en langage Python


1. [France IOI](http://www.france-ioi.org)  :
    *  Niveau 1, chapitre 3 _Répétition d'instructions_: 
       *  Exercice [Course avec les enfants](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=18), imbriquer des répétitions
       *  Exercice [Construction d'une pyramide](http://www.france-ioi.org/algo/chapter.php?idChapter=644), boucle et accumulateur
       *  Exercice [Table de multiplication](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=20), boucles imbriquées
    
    

2. On termine le  [TP2](../chapitre1/TP2/TP2.pdf)  (partie fonctions) en s'aidant de la synthèse de cours : [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés :
    * ➡️ _Bases d'un langage de programmation : instructions, littéraux, expressions_, exemple d'un programme avec entrée/sortie (calcul d'âge à partir de la date de naissance)
     * ➡️ _Instructions conditionnelles_ :  Exercice 5 p. 42, QCM question 6 p. 39
    * ➡️ _Boucles bornées_ 
    * ➡️ _Boucles non bornées_
    * ➡️ _Fonctions_

    Ressources pour le [TP2](../chapitre1/TP2/TP2.pdf)  :
        Liens vers les corrigés :

    * [corrigé pdf](../chapitre1/TP2/correction/Correction_TP2.pdf)
    * [corrigé .ipynb](../chapitre1/TP2/correction/Correction_TP2.ipynb)
    * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)
     
3. Exercices  d'entraînement :  à partir de l'exercice <https://frederic-junier.gitlab.io/parc-nsi/automatismes/variables/exercice1_variables/> traiter tous les exercices suivants (tests, boucles `for`, boucles `while`, fonctions ...) jusqu'à <https://frederic-junier.gitlab.io/parc-nsi/automatismes/fonctions/exo5_fonctions_produit_for/>


4. Travail sur le [MiniProjet1](../Projets/MiniProjets2022-2023/NSI-MiniProjets1-2022-2023-Sujets.pdf), inscription par binôme sur <https://nuage03.apps.education.fr/index.php/s/WYT8ED4GL8NDAZ3>. A rendre pour le vendredi 23/09 dans la zone de dépôt sur Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=964>

5. Un [pydéfi](https://pydefis.callicode.fr)  :
   
   * Énoncé :  [Entrée au ministère](https://pydefis.callicode.fr/defis/CodeCabine/txt)
   * Fichier Capytale : <https://capytale2.ac-paris.fr/web/c/3114-676819/mcer>
   * Solutions Capytale :  <https://capytale2.ac-paris.fr/web/c/6c98-676789/mcer>


## A faire pour mardi 16/09 :

1. Sur <http://www.france-ioi.org>  faire jusqu'à l'exercice 4 le  __niveau 1  chapitre 3(calculs)__  

2. Travailler sur son  mini-projet : énoncé sur [MiniProjet1](../Projets/MiniProjets2022-2023/NSI-MiniProjets1-2022-2023-Sujets.pdf), inscription par binôme sur <https://nuage03.apps.education.fr/index.php/s/WYT8ED4GL8NDAZ3>. A rendre pour le mardi 27/09 dans la zone de dépôt sur Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=964>
3. Jeudi 22/09 : interrogation écrite sur les constructions de base en Python (tableau d'évolution des variables, tests, boucles, fonctions)



