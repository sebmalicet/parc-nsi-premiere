---
title: Séance du 16/03/2023
---

# Séance du 16/03/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé
## DS 8 : thème : dictionnaires

## Chapitre Flottants

* [Cours version pdf](../chapitre16/1NSI-Cours-Flottants-2021V1.pdf)
* [Carnet Capytale  des exemples du cours, avec corrigés](https://capytale2.ac-paris.fr/web/c/78a9-386046)



## Présentation du module pyxel et du projet de fin d'année

* [Tutoriel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)



## Travail à faire

* Pour la prochaine séance, choisir un sujet pour le projet final, former un binôme, s'inscrire dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
