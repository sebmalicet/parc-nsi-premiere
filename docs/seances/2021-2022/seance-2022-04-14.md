---
title: Séance du 14/04/2022
---

# Séance du 14/04/2022


## TP algorithmes gloutons


* [Énoncé pdf](../chapitre24/TP-Gloutons-2022.pdf)
* [Matériel : squelette de code, fichiers csv, corrigé](../chapitre24/materiel.zip)
* Aujourd'hui on traite le rendu de monnaie glouton


## Présentation du projet de fin d'année, découverte du module pyxel


* [Tutoriel sur le module pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document de cadrage du projet final](../Projets/2021-2022/ProjetFinal/Cadrage/NSI_Presentation_Projet2022.pdf)
* [Inscription avec le mot de passe donné par l'enseignant](https://nuage-lyon.beta.education.fr/s/Fyf7EQgrpHSfCFP)

