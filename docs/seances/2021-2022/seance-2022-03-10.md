---
title: Séance du 10/03/2022
---

# Séance du 010/03/2022

## Chapitre Dictionnaires


* [Cours](../chapitre18/Cours/dictionnaires-cours-git.md)
* Exercices 2, 3 et 4 du cours + exercices sur le site des collègues de NSI <https://e-nsi.gitlab.io/nsi-pratique/N1/antecedents/sujet/> ➡️  [Corrigés dans ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180).



## Travail sur le projet sutom


* Date de remise sur le Moodle :  le samedi 19 mars 2022, [projet sutom](../projets.md)


## DS n°7