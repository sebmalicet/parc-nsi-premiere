---
title: Séance du 09/03/2022
---

# Séance du 09/03/2022



## Chapitre architecture de Von Neumann 

* [Lien vers le chapitre 17](../chapitre17.md)
* [Cours version pdf](../chapitre17/NSI-ArchitectureVonNeumann-Cours2020V2.pdf)
* __Premier temps :__ histoire de l'informatique et visionnage de quelques séquences de la video <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs> avec application aux exercices 1 et 2
* __Second temps (cours) :__ présentation de l'architecture de Von Neummann, de la hiérarchie des mémoires, des différents niveaux de langage entre l'homme et la machine :  faire l'exercice 3 mais pas les 4 et 5. 
* __Troisième temps (pratique) :__  Manipulation de l'[Emulateur Aqua](http://www.peterhigginson.co.uk/AQA/)
    * Ressources :
      * [Correction video de l'exercice 7](https://nuage-lyon.beta.education.fr/s/x5dN4TtFgqzJMQY)
      * [Correction video de l'exercice 10](https://nuage-lyon.beta.education.fr/s/BbRCqcmRjiqjAgo)


## Travail sur le projet sutom


* Date de remise sur le Moodle :  le samedi 19 mars 2022, [projet sutom](../projets.md)


## Travail pour demain :

DS n°7