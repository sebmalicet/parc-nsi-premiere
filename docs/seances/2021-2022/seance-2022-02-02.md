---
title: Séance du 02/02/2022
---

# Séance du 02/02/2022

## Automatismes

* [Automatisme 8](../automatismes/automatismes-2021-2022.md)
* Révisions sur l'implémentation de la recherche dichotomique dans un tableau trié et du tri par sélection en place.


## Chapitre Complexité


* [Cours version pdf](../chapitre14/cours_complexite_2022.pdf)
* [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/38c6-329322)

## Evaluation sommative du jeudi 03/02

Réviser les chapitres recherche séquentielle et dichotomique, tris et chaînes de caractères.

## Travail sur le projet sutom


* Date de remise sur le Moodle :  le samedi 19 mars 2022, [projet sutom](../projets.md)


