---
title: Séance du 19/01/2022
---

# Séance du 19/01/2022

## Automatismes :

* [Automatisme 29, tri par insertion](../automatismes/automatismes-2021-2022.md)

## Chapitre 10 : codage des caractères

Présentation des notions de table de codage, de point de code, d'encodage. 
Les chaînes de caractères du type `str` en Python : points communs avec les tableaux de type `list` et différences (immutabilité).

Exercice 1  sur l'inversion d'une chaîne et la détection de palindrome.

* [Cours version pdf](../chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
* [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/9192-288733)


![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)


## Travail sur le projet brainfuck

* Questions/réponses.

* Date de remise : pour le samedi 29 Janvier 2022, [projet brainfuck](../projets.md)


??? video  "Vidéo tutoriel 1"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="branfuck_partie1" src="https://tube.ac-lyon.fr/videos/embed/d97f5c67-f791-43d3-bb96-ddf6f4eb5be3" frameborder="0" allowfullscreen></iframe>



??? video  "Vidéo tutoriel 2"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="brainfuck_partie2" src="https://tube.ac-lyon.fr/videos/embed/9e135a4e-3416-4d6d-b4b1-2525ba2f1923" frameborder="0" allowfullscreen></iframe>

