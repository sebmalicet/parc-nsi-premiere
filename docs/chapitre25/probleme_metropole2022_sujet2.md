??? info "Rappels"

    * Une adresse IPv4 est composée de 4 octets, soit 32 bits. Elle est notée a.b.c.d, où a, b, c et d sont les valeurs des 4 octets.
    *  La notation a.b.c.d/n signifie que les n premiers bits de l'adresse IP représentent la partie " réseau ", les bits qui suivent représentent la partie " machine ".
    * L'adresse IPv4 dont tous les bits de la partie " machine " sont à 0 est appelée  " adresse du réseau ".
    * L'adresse IPv4 dont tous les bits de la partie " machine " sont à 1 est appelée " adresse de diffusion ".

On considère le réseau représenté sur la Figure 1 ci-dessous :

![alt](images/reseau.png)

On peut le modéliser par un graphe, objet mathématique où chaque routeur s'appelle un *sommet* du graphe
et chaque liaison  une *arête*.

![alt](images/graphe.png)


!!! tip "Question 1 : plan d'adressage"

    === "Énoncé"
        On considère la machine d'adresse IPv4 192.168.1.1/24

        1. Donner l'adresse du réseau sur lequel se trouve cette machine.
        2. Donner l'adresse de diffusion (broadcast) de ce réseau.
        3. Donner le nombre maximal de machines que l'on peut connecter sur ce réseau.
        4. On souhaite ajouter une machine sur ce réseau, proposer une adresse IPv4
        possible pour cette machine.

    === "Solution"

        1. L'adresse du réseau sur lequel se trouve cette machine est 192.168.1.0
        2. L'adresse de diffusion (broadcast) de ce réseau est 192.168.1.255
        3. Pour déterminer nombre maximal de machines que l'on peut connecter sur ce réseau, on calcule le nombre total d'adresses possibles sur $8=32-24$ bits soit $2^8=256$ auquel on retranche l'adresse du réseau et l'adresse de diffusion. Cela nous donne $256 - 2=254$.
        4. Une  adresse IPv4 possible pour cette machine est $192.168.1.2$

!!! tip "Question 2 : routage"

    === "Énoncé"

        1. La machine d'adresse IPv4 192.168.1.1 (de passerelle le routeur A) transmet un paquet IPv4 à la machine d'adresse IPv4 192.168.4.2 (de passerelle le routeur D). Donnez toutes les routes pouvant être empruntées par ce paquet IPv4, chaque routeur ne pouvant être traversé qu'une seule fois.
        2. Expliquer l'utilité d'avoir plusieurs routes possibles reliant les réseaux 192.168.1.0/24 et 192.168.4.0/24


    === "Solution"

        1. Toutes possibles :
        
            A -> B -> C -> F -> D

            A -> B -> C -> E -> D

            A -> E -> C -> F -> D

            A -> E -> D

            A -> C -> E -> D

            A -> C -> F -> D

        2. Il est utile d'avoir plusieurs routes possibles en cas de panne d'un des routeurs intermédiaires B, C, E ou F.


!!! tip "Question 3 : protocole de routage RIP"

    === "Énoncé"
        Dans cette question, on suppose que le protocole de routage mis en place dans le réseau est RIP. Ce protocole consiste à minimiser le nombre de sauts. Le schéma du réseau est celui de la figure 1. Les tables de routage utilisées sont données ci-dessous :

        ![alt](images/tables_routage.png)

        1. Recopier et compléter sur la copie la table de routage du routeur A.
        2. Un paquet IP doit aller du routeur B au routeur D. En utilisant les tables de routage, donner le parcours emprunté par celui-ci.
        3. Les connexions entre les routeurs B-C et A-E étant coupées, sur la copie, réécrire les tables de routage des routeurs A, B et C.
        4. Déterminer le nouveau parcours emprunté par le paquet IP pour aller du routeur B au routeur D.

    === "Solution"

        1. Table du routeur A :
       
        |Destination|passe par|
        |:--:|:--:|
        |B|B|
        |C|C|
        |D|E|
        |E|E|
        |F|C|

        2. Le parcours emprunté par un paquet pour aller  du routeur B au routeur D est ==B -> C -> E -> D==.       
        3. Les connexions entre les routeurs B-C et A-E étant coupées, sur la copie,
        réécrire les tables de routage des routeurs A, B et C.        
        Nouveau graphe :
        ![alt](images/graphe2.png)       
        Mise à jour des tables de routage (protocole RIP, on minimise le nombre de "sauts"/arêtes entre origine et destination) :
        Table du routeur A :

        |Destination|passe par|
        |:--:|:--:|
        |B|B|
        |C|C|
        |D|C|
        |E|C|
        |F|C|

        Table du routeur B :

        |Destination|passe par|
        |:--:|:--:|
        |A|A|
        |C|A|
        |D|A|
        |E|A|
        |F|A|

        Table du routeur C :

        |Destination|passe par|
        |:--:|:--:|
        |A|A|
        |B|A|
        |D|E|
        |E|E|
        |F|F|

        4. Le nouveau parcours emprunté par le paquet IP pour aller du routeur B au routeur D est :   
        ==B -> A -> C -> E -> D==
            
!!! tip "Question 4 : protocole de OSPF"

    === "Énoncé"
        Dans cette question, on suppose que le protocole de routage mis en place dans le réseau est OSPF. Ce protocole consiste à minimiser la somme des coûts des liaisons empruntées. Le coût d'une liaison est défini par la relation coût = $\frac{10^{8}}{d}$ où $d$ représente le débit en bit/s et coût est sans unité. Le schéma du réseau est celui de la figure 1.

        1. Déterminer le coût des liaisons Ethernet ($d = 10^7$ bit/s ), Fast-Ethernet ($d = 10^8$ bit/s ) et Fibre ($d = 10^9$ bit/s ).
        2. Recopier sur la copie le schéma ci-dessous et tracer les liaisons entre les routeurs en y indiquant le coût.
        ![alt](images/ospf1.png)        
        3. Un paquet IPv4 doit être acheminé d'une machine ayant pour adresse IPv4 192.168.2.1 (de passerelle le routeur B) à une machine ayant pour adresse IPv4 192.168.4.1 (de passerelle le routeur D). Écrire les routes possibles, c'est à dire la liste des routeurs traversés, et le coût de chacune de ces routes, chaque routeur ne pouvant être traversé qu'une seule fois.       
        4. Donner, en la justifiant, la route qui sera empruntée par un paquet IPv4 pour aller d'une machine ayant pour adresse IPv4 192.168.2.1 à une machine ayant pour adresse IPv4 192.168.4.1

    === "Solution"

        1. Coûts des liaisons :   
            - Le coût d'une liaison Ethernet est de $\frac{10^{8}}{10^7}=10$.
            - Le coût d'une liaison Fast-Ethernet est de $\frac{10^{8}}{10^8}=1$.
            - Le coût d'une liaison Fibre est de $\frac{10^{8}}{10^9}=0,1$.  
        2. Graphe pondéré par le coût des liaisons :   
        ![alt](images/ospf2.png)
        3. Coûts des routes :   
   
        |Route|Coût|
        |:---:|:---:|
        |B->A->C->E->D|1.3|
        |B->A->C->F->D|12.1|
        |B->A->E->D|2.1|
        |B->C->E->D|10.2|
        |B->C->F->D|11.1|

        4. Le protocole OSPF choisit la route coût minimal donc B->A->C->E->D pour un coût de $1,3$.