#!/bin/bash
base="http://quandjepasselebac.education.fr/ec/BNS/Bac%20G%C3%A9n%C3%A9ral/Premi%C3%A8re/Enseignements%20de%20sp%C3%A9cialit%C3%A9/Sp%C3%A9cialit%C3%A9%20num%C3%A9rique%20et%20sciences%20informatiques/ec-2/G1SNSIN0"
i=1
j=3316
bsup=199
while [ "$i" -lt "$bsup" ] ; do
    case $j in 
    3330)   j=3331  ;;
    3336)   j=3337 ;;
    3343)   j=3351 ;;
    3377)   j=5008 
            i=1
            ;;
    esac
    wget ${base}$((j))"-sujet"${i}'.pdf'
    ((i++))
    ((j++))
done