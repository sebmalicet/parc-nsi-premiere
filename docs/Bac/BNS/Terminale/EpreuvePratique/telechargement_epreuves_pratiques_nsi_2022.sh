#!/bin/bash
base="https://eduscol.education.fr/document/"
i=0
j=33178
bsup=41
while [ "$i" -lt "$bsup" ] ; do
    wget ${base}$((j))"/download"
    unzip download
    ls download | xargs -I% mv % ..
    rm -r download
    ((i++))
    j=$(($j+3))
done


