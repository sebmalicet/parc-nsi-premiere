---
title: Thème Algorithmes gloutons
---

{% include 'abbreviations.md' %}

!!! tip "Exercice"

    _Sujet inspiré de <https://prologin.org/train/2023/qualification/mise_en_boites>_

    Cyril et 5 de ses amis sont en train de choisir leur film quand Cyril se rend compte que les restes du repas qu'ils ont dégusté juste avant attendent encore sur la table basse. Pour conserver leurs aliments, il faut les mettre dans des boîtes et les ranger au réfrigérateur. Qui sait, ils pourraient en avoir besoin si d'aventure ils décidaient de se lancer dans un grand voyage ! Le hasard faisant bien les choses, ils ont autant de boîtes que de restes. Tous les récipients ne sont toutefois pas forcément de la bonne taille.

    Aidez nos 6 amis à remplir le plus de boîtes sachant qu'un aliment d'un certain volume ne peut entrer que dans une boîte d'un volume supérieur ou égal. Afin de conserver au mieux le goût de la nourriture, on ne mettra qu'un aliment dans chaque boîte.

    Complétez dans `tp12_vers_13.py`, la fonction `mise_en_boite(restes, boites)` pour que sa spécification et les tests unitaires fournis soient satisfaits.

    ```python
    def mise_en_boite(restes, boites):
        """
        Renvoie le nombre maximal de boites pour placer chaque reste
        d'aliment   dans une boite qui peut le contenir

        Paramètres :
        restes : liste de volumes d'aliments (entiers positifs)
        boites : liste de volumes de boites (entiers positifs)

        Retour: un entier positif
        """
    ```

    Exemple 1 :

    ```python
    >>> mise_en_boite([832, 342, 500], [750, 250, 500])
    2
    ```

    Ici on peut mettre l'aliment de 500 cm³ dans la boîte de 500 cm³. Le reste de 342 cm³ peut être rangé dans la boîte de 750 cm³. On peut aussi mettre le reste de 342 cm³ dans la boîte de 500 cm³ et celui de 500 cm³ dans la boîte de 750 cm³.

    Exemple 2 :

    ```python
    >>> mise_en_boite([487, 601, 584, 819, 601], [750, 500, 700, 65, 700])
    4
    ```

    Voici une liste possible de paires (reste, boîte) : (487, 500), (584, 650), (601, 700), (601, 700). On aurait pu également mettre un aliment de 601 cm³ dans la boîte de 750 cm³. Le mets de 819 cm³ ne peut être mis en boîte.

{{IDE("exo1_gloutons")}}

[Correction](exo1_gloutons.py)
