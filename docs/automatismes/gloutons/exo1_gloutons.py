def mise_en_boite(restes, boites):
    """
    Renvoie le nombre maximal de boites pour placer chaque reste
    d'aliment   dans une boite qui peut le contenir

    Paramètres :
    restes : liste de volume d'aliments (entiers positifs)
    boites : liste de volume de boite (entiers positifs)

    Retour: un entier
    """
    # à compléter


def test_mise_en_boite():
    """Tests unitaires pour mise en boite"""
    restes1 = [832, 342, 500]
    boites1 = [750, 250, 500]
    assert mise_en_boite(restes1, boites1) == 2
    restes2 = [487, 601, 584, 819, 601]
    boites2 = [750, 500, 700, 65, 700]
    assert mise_en_boite(restes2, boites2) == 4
    print("Tests réussis pour mise_en_boite")
