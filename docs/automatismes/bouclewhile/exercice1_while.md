---
title: Thème boucle while
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de quatre lignes de code au plus, qui affiche le plus petit entier positif $n$ tel $n^{2}-1000n-10000 \geqslant 10^{6}$.
    


{{IDE("exo1_while")}} 

