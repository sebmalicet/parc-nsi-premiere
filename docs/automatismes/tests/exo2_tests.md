---
title: Thème Tests
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Compléter le programme Python ci-dessous pour qu'il affiche :  `"mineur"` si   `0 <= age < 18`,  `"majeur dans l'année"` si  `age == 18`,  `"majeur"` si `18 < age <= 130` et  `"valeur incohérente"` sinon.
            


{{IDE("exo2_tests")}} 



[Correction](corr_exo2_tests.py)
