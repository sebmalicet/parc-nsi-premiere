---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python qui affiche "`toutes égales`" si les valeurs des variables `a`, `b` et `c` de type `int` sont toutes égales et  "`pas toutes égales`" sinon.
    


{{IDE("exo3_tests")}} 

[Correction](corr_exo3_tests.py)
