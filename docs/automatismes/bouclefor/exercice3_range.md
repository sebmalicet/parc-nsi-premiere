---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de deux lignes de code au plus,  qui affiche tous les entiers entre 10 et 0 inclus dans l'ordre décroissant (un par ligne).
    


{{IDE("exo3_range")}} 

[Correction](exo3_range_corr.py)
