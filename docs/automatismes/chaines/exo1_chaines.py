def compare(chaine1, chaine2):
    """
    Compare deux chaînes de caractères et renvoie la plus petite
    dans l'ordre lexicographique

    Paramètres :
    chaine1 et chaine2 sont deux chaînes de caractères composés
    uniquement de caractères minuscules.

    Retour: la chaine la plus petite dans l'ordre lexicographique
    """

assert compare('informatique', 'information') == 'information'
assert compare('pyramide', 'python') == 'pyramide'
assert compare('courge', 'courgette') == 'courge'
