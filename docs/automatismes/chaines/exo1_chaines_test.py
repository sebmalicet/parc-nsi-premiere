# Tests de base


# test 1
assert compare('informatique', 'information') == 'information'

# test 2
assert compare('pyramide', 'python') == 'pyramide'

# test 3
assert compare('courge', 'courgette') == 'courge'


# Test cachés

# test 4
assert compare('', 'courgette') == ''
# test 5
assert compare('fille', 'fille') == 'fille'
