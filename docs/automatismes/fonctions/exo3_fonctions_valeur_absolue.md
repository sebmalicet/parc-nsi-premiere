---
title: Thème Fonctions, tests
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Compléter la fonction `valeur_absolue` ci-dessous qui prend en paramètre un nombre `x` et renvoie `x` si `x >= 0` et `-x` sinon.
        
    ~~~python
    def valeur_absolue(x): 
        """Signature valeur_absolue(x:float)->float"""  
        #à compléter
    ~~~


{{IDE("exo3_fonctions")}} 
