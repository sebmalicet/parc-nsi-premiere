author: Frédéric Junier d'après https://www.codingame.com/ide/puzzle/scrabble
title: Scrabble 
tags: dictionnaire
---



{% include 'abbreviations.md' %}

!!! tip "Exercice"

    Exercice inspiré de  <https://www.codingame.com/ide/puzzle/scrabble>.


    Au jeu de [Scrabble](https://fr.wikipedia.org/wiki/Scrabble) on tire sept lettres (avec répétition possible de la même lettre) et on doit former à partir de ces lettres le mot du dictionnaire qui rapporte le plus de points. Un barème affecte en effet à chaque lettre une valeur en points. 

    ~~~python
    VALEUR = {
    "a": 1,
    "e": 1,
    "i": 1,
    "o": 1,
    "n": 1,
    "r": 1,
    "t": 1,
    "l": 1,
    "s": 1,
    "u": 1,
    "d": 2,
    "g": 2,
    "b": 3,
    "c": 3,
    "m": 3,
    "p": 3,
    "f": 4,
    "h": 4,
    "v": 4,
    "w": 4,
    "y": 4,
    "k": 5,
    "j": 8,
    "x": 8,
    "q": 10,
    "z": 10,
    }
    ~~~

    Dans cet exercice, on considère uniquement les 26 lettres minuscules de l'alphabet latin et la valeur de chaque lettre est définie dans un dictionnaire Python noté `VALEUR` car c'est une variable globale du programme.

    Le but de l'exercice est d'écrire une fonction `recherche_mot_max(lettres, liste_mots)` qui renvoie le mot de score maximal dans la liste de mots `lsite_mots` qui peut être écrit à partir des 7 caractères de `lettres`.  En cas d'égalité entre deux mots, la fonction renverra le plus grand dans l'ordre alphabétique.
        
    ~~~python
    def recherche_mot_max(lettres, liste_mots):
        """
        Renvoie le mot de liste_mots avec le score maximal pour les lettres.

        Parameters
        ----------
        lettres : chaine de caractères (str) avec les lettres tirées  sans espaces
        liste_mots : tableau de mots (type str) possibles

        Returns
        -------
        mot_max : mot réalisant le  score maximal de type str
        """
        # à compléter


    liste_mots = [
        "amidons",
        "captes",
        "chatier",
        "empeser",
        "hatez",
        "missive",
        "missile",
        "defige",
        "raison",
        "raz",
    ]
    assert recherche_mot_max("airsonz", liste_mots) == "raz"
    assert recherche_mot_max("chatierz", liste_mots) == "hatez"
    ~~~

    Vous allez d'abord écrire trois  fonctions outils :

    1. `signature(chaine)` prend en paramètre une chaîne de caractères et renvoie sa signature sous la forme de l'histogramme de ses caractères, dictionnaire associant à chaque caractère son nombre d'occurrence dans `chaine`.

        ~~~python
        def signature(chaine):
            """
            Renvoie l'histogramme des lettres composant chaine.

            Parameters
            ----------
            mot : chaine de caractères de type str

            Returns
            -------
            sig : dictionnaire (clef=lettre, valeur=effectf)

            """
            # à compléter


        assert signature("") == {}
        assert signature("ananas") == {"a": 3, "n": 2, "s": 1}
        assert signature("abcd") == {"a": 1, "b": 1, "c": 1, "d": 1}
        ~~~

    2. `score_mot(mot)` prend en paramètre un mot de type `str`, et renvoie son score calculé à partir du barème enregistré dans le dictionnaire `VALEUR` qui est une variable globale.

        ~~~python
        def score_mot(mot):
            """
            Renvoie le score d'un mot.

            Parameters
            ----------
            mot : mot de type str

            Returns
            -------
            s : score du mot selon le barème VALEUR

            """
            # à compléter

        assert score_mot("") == 0
        assert score_mot("zazou") == 23
        assert score_mot("ananas") == 6
        ~~~

    3. `mot_possible(mot, sig_lettres)` prend en paramètres `mot` de  type `str` et `sig_lettres` un dictionnaire  caractérise un tirage de 7 lettres sous forme d'histogramme de caractères, et détermine si on peut composer  `mot`  à partir des caractères de ce tirage.

        ~~~python
        def mot_possible(mot, sig_lettres):
            """
            Détermine si on peut écrire mot avec l'histogramme sig_lettres.

            Parameters
            ----------
            mot : mot de type str
            sig_lettres : histogramme de lettres
                        sous forme de dictionnaire (clef=lettre, valeur=effectf)

            Returns
            -------
            booléen
            """
            # à compléter


        assert mot_possible("zazou", {"a": 1, "b": 1, "o": 2, "u": 1, "z": 2})
        assert not mot_possible("zazou", {"b": 1, "o": 1, "u": 1, "z": 2})
        assert not mot_possible("zazou", {"a": 1, "o": 2, "u": 3, "z": 1})
        ~~~


{{IDE("scrabble/exo_scrabble")}} 