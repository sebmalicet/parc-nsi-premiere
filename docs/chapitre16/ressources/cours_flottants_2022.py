#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Cours Flottants Première NSI Lycée du Parc 20222"""


# %% Conversion d'un entier positif en base 2
def entier_vers_binaire(n):
    """
    Renvoie le tableau de bits de la représentation de n en base 2.

    Parameters
    ----------
    n : (int) entier
    Précondition n >= 0

    Returns
    -------
    tableau de bits représentant la conversion de n en binaire
    bits de poids forts à gauche
    """
    assert n >= 0
    tab_bits = []
    while n >= 2:
        tab_bits = [n % 2] + tab_bits
        n = n // 2
    tab_bits = [n] + tab_bits
    return tab_bits


def test_entier_vers_binaire():
    """Tests unitaires pour entier_vers_binaire."""
    for n in range(100):
        chaine_binaire = '0b' + ''.join([str(b) for b in entier_vers_binaire(n)])
        assert chaine_binaire == bin(n)
    print("Tests réussis")


def binaire_vers_entier(tab_bits):
    """
    Convertit en entier décimal un tableau de bits de  représentation binaire.

    Parameters
    ----------
    tab_bits : tableau de bits (0 ou 1)

    Returns
    -------
    (int) entier
    """
    n = 0
    for bit in tab_bits:
        n = n * 2 + bit
    return n


def test_binaire_vers_entier():
    """Tests unitaires pour binaire_vers_entier."""
    for n in range(100):
        chaine_binaire = bin(n)
        tab_bits = [int(b) for b in chaine_binaire.lstrip('0b')]
        assert binaire_vers_entier(tab_bits) == n
    print("Tests réussis")


def bourrage_zero_gauche(tab_bits, nb_bits):
    """
    Remplit par des 0 à gauche tab_bits jusqu'à une longueur de nb_bits.

    Parameters
    ----------
    tab_bits : tableau de bits (0 ou 1)
    précondition :  len(tab_bits) <= nb_bits
    nb_bits : (int) taille du tableau cible

    Returns
    -------
    tableau de bits (0 ou 1) de taille nb_bits.
    """
    assert len(tab_bits) <= nb_bits
    return [0 for _ in range(max(0, nb_bits - len(tab_bits)))] + tab_bits


# %% Conversion d'une partie fractionnaire en binaire sur nb_bits
def partie_frac_vers_binaire(partie_frac, nb_bits):
    """
    Renvoie la représentation en base 2 sur nb_bits de 0 <= partie_frac < 1.

    Parameters
    ----------
    partie_frac : (float) un réel >= 0 et < 1
    nb_bits : (int)  un nombre de bits pour la représentation

    Returns
    -------
    tableau de bits de nb_bits bits
    """
    tab_bits = []
    for _ in range(nb_bits):
        decalage = 2 * partie_frac
        bit = int(decalage)
        tab_bits.append(bit)
        partie_frac = decalage - bit
    return tab_bits


def test_partie_frac_vers_binaire():
    """Tests unitaires pour partie_frac_vers_binaire."""
    assert partie_frac_vers_binaire(0.3, 10) == [0, 1, 0, 0, 1, 1, 0, 0, 1, 1]
    assert partie_frac_vers_binaire(0.2, 10) == [0, 0, 1, 1, 0, 0, 1, 1, 0, 0]
    assert partie_frac_vers_binaire(0.25, 2) == [0, 1]
    assert partie_frac_vers_binaire(0.5, 1) == [1]
    print("Tests réussis")


def binaire_vers_partie_frac(tab_bits):
    """
    Renvoie la partie fractionnaire décimale représentée par tab_bits.

    Parameters
    ----------
    tab_bits : tableau de bits (0 ou 1)

    Returns
    -------
    f : (float) partie fractionnaire représentée par tab_bits
    """
    f = 0
    d = 2
    for bit in tab_bits:
        f = f + bit / d
        d = d * 2
    return f


def test_binaire_vers_partie_frac():
    """Tests unitaires pour binaire_vers_partie_frac."""
    assert binaire_vers_partie_frac([0, 1, 0, 0, 1, 1, 0, 0, 1, 1]) == 0.2998046875
    assert binaire_vers_partie_frac([0, 0, 1, 1, 0, 0, 1, 1, 0, 0]) == 0.19921875
    assert binaire_vers_partie_frac([0, 1]) == 0.25
    assert binaire_vers_partie_frac([1]) == 0.5
    print("Tests réussis")


# %% Calcul de l'exposant dans la représentation d'un flottant
def calcul_exposant(d):
    """
    Renvoie le plus grand entier relatif e tel que 2 ** e <= d.

    Parameters
    ----------
    d : (float) décimal positif non nul

    Returns
    -------
    e : (int)
    """
    assert d != 0
    e = 0
    while d < 1:
        d = d * 2
        e = e - 1
    while d >= 2:
        d = d / 2
        e = e + 1
    return e


def test_calcul_exposant():
    """Tests unitaires pour calcul_exposant."""
    assert calcul_exposant(1) == 0
    assert calcul_exposant(1.4) == 0
    assert calcul_exposant(2) == 1
    assert calcul_exposant(7) == 2
    assert calcul_exposant(8) == 3
    assert calcul_exposant(15.7) == 3
    assert calcul_exposant(16) == 4
    assert calcul_exposant(0.6) == -1
    assert calcul_exposant(0.5) == -1
    assert calcul_exposant(0.3) == -2
    assert calcul_exposant(0.25) == -2
    print("Tests réussis")


# %% Renvoie représentation iee754 du décimal d sur 32 bits
def decimal_vers_float32(d):
    """
    Renvoie un tableau de 32 bits représentant le décimal  au format float32.

    Parameters
    ----------
    d : (float) un décimal

    Returns
    -------
    tableau de bits
    """
    if d == 0:  # cas particulier du 0
        return [0 for _ in range(32)]
    if d >= 0:
        signe = [0]
    else:
        signe = [1]
    exposant = calcul_exposant(abs(d))
    if (exposant + 127 < 0) or (exposant + 127 > 255):
        raise OverflowError()
    # on bourre avec  des 0 à gauche
    exposant_decale = bourrage_zero_gauche(entier_vers_binaire(exposant + 127), 8)
    val_abs = abs(d)
    partie_entiere = int(val_abs)
    partie_frac = val_abs - partie_entiere
    if exposant >= 0:
        bits_partie_entiere = entier_vers_binaire(partie_entiere)
        nb_bits_partie_entiere = min(23, len(bits_partie_entiere) - 1)
        bits_partie_frac = partie_frac_vers_binaire(partie_frac, 23 - nb_bits_partie_entiere)
        mantisse = bits_partie_entiere[1:nb_bits_partie_entiere + 1] + bits_partie_frac
    else:
        bits_partie_frac = partie_frac_vers_binaire(partie_frac, 23 - exposant)
        mantisse = bits_partie_frac[-exposant:-exposant + 23]
    return signe + exposant_decale + mantisse

def str_decimal_vers_float32(d):
    """
    Renvoie une chaine de caractère représentant le décimal  au format float32.

    Parameters
    ----------
    d : (float) un décimal

    Returns
    -------
    (str) Représentation de d au format float32
    """
    return ''.join([str(b) for b in decimal_vers_float32(d)])


def test_decimal_vers_float32():
    """Tests unitaires pour decimal_vers_float32."""
    assert str_decimal_vers_float32(-10.3) == '11000001001001001100110011001100'
    assert str_decimal_vers_float32(0.1) == '00111101110011001100110011001100'
    assert str_decimal_vers_float32(2) == '01000000000000000000000000000000'
    assert str_decimal_vers_float32(2 ** 127) == '01111111000000000000000000000000'
    assert str_decimal_vers_float32(0) == '00000000000000000000000000000000'
    assert str_decimal_vers_float32(2 ** 24) == str_decimal_vers_float32(2 ** 24 + 1)
    print("Tests réussis")


def float32_vers_decimal(tab_bits):
    """
    Renvoie un  décimal de représentation float32 tab_bits.

    Parameters
    ----------
    tab_bits : tableau de bits (0 ou 1)

    Returns
    -------
    (float)
    """
    if tab_bits[0] == 0:
        signe = 1
    else:
        signe = -1
    exposant_decale = binaire_vers_entier(tab_bits[1:9])
    exposant = exposant_decale - 127
    mantisse_frac = binaire_vers_partie_frac(tab_bits[9:])
    if (exposant_decale == 0):
        return 0
    if (exposant_decale == 255) and (mantisse_frac == 0):
        return signe * float('inf')
    if (exposant_decale == 255) and (mantisse_frac != 0):
        return float('nan')
    mantisse = 1 + mantisse_frac
    return signe * 2 ** (exposant) * mantisse


def test_float32_vers_decimal():
    """Tests unitaires pour float32_vers_decimal."""
    assert float32_vers_decimal(decimal_vers_float32(0.25)) == 0.25
    assert float32_vers_decimal(decimal_vers_float32(-10.625)) == -10.625
    t_zero_pos = [0] * 32
    assert float32_vers_decimal(t_zero_pos) == float(0)
    t_zero_neg = [1] + [0] * 31
    assert float32_vers_decimal(t_zero_neg) == float(0)
    t_inf_pos = [0] + [1] * 8 + [0] * 23
    assert float32_vers_decimal(t_inf_pos) == float('inf')
    t_inf_neg = [1] + [1] * 8 + [0] * 23
    assert float32_vers_decimal(t_inf_neg) == -float('inf')
    t_nan = [0] + [1] * 8 + [1] * 23
    assert float32_vers_decimal(t_nan) == float('nan')
    print("Tests réussis")