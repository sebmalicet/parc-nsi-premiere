---
title:  Sujets d'exposés sur la culture informatique
layout: parc
---


|Domaine|Compétence|Barème|
|:---:|:---:|:---:|
|*Qualité des connaissances*||**../5 points**|
||Traitement du sujet|../1 point|
||Précision du vocabulaire|../1 point|
||Clarté des explications|../1 point|
||Contenu adapté au public|../1 point|
||Citation des sources|../1 point|
|*Qualité et construction de l'argumentation*||**../5 points**|
||Plan structuré|../1 point|
||Question, problématique ou ligne directrice|../1 point|
||Transitions, connecteurs logiques entre parties|../1 point|
||Progression dans l'argumentation|1 point|
||Raisonnement logique|../1 point|
|*Qualité de la prise de parole en continue*||**../3 points**|
||Débit adapté|../1 point|
||Fluidité du discours, reprises, hésitations|../1 point|
||Respects des contraintes de temps|../1 point|
|*Qualité orale*||**../5 points**|
||Niveau de voix adapté|../1 point|
||Assurance de la parole|../1 point|
||Engagement non verbal, visage|../1 point|
||Engagement non verbal, regard|../1 point|
||Engagement non verbal, tronc, jambes, posture|../1 point|
|*Qualité de l'interaction (questions)*||**../2 points**|
||Réactivité|../1 point|
||Maîtrise des connaissances|../1 point|
|*Total*||**../20 points**|

