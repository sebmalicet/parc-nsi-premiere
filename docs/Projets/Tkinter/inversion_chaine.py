from tkinter import *

def action():
    sortie["text"] = source.get()[::-1]
    
fen = Tk()
cadre1 = Frame(fen)
cadre1.pack(side="left")
label1 = Label(cadre1, text="SOURCE", 
               width=30, bg='red')
label1.pack(side="top")
source = StringVar()
saisie = Entry(cadre1, textvariable=source)
saisie.pack(side="top")
bouton = Button(fen, text="Codage!",
                command=action)
bouton.pack(side="left")
cadre2 = Frame(fen)
cadre2.pack(side="left")
label2 = Label(cadre2, text="SORTIE",
               width=30, bg="#baf5c7")
label2.pack(side="top")
sortie = Label(cadre2)
sortie.pack(side="top")
fen.mainloop()