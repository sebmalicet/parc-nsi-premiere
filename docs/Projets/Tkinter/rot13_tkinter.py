import tkinter as tk


##------- Fonctions  -------##
def rot13(chaine):
    """Renvoie le codage de chaine en rot13"""
    res = ""
    chaine = chaine.upper()
    for c in chaine:
        if ord("A") <= ord(c) <= ord("Z"):
            res = res + chr((ord(c) - ord("A") + 13) % 26 + ord("A"))
        else:
            res = res + c
    return res


def codage_rot13():
    """Commande le codage en rot13 du texte source et son affichage sur la sortie"""
    affichage_sortie["text"] = rot13(source.get())


##------- Création de la fenêtre racine -------##

fenetre = tk.Tk()
fenetre.title("Codage Rot13")


## -------  Variables globales et variables de contrôle  -------

##------- Cadre pour la source -------##
cadre_source = tk.Frame(fenetre)
cadre_source.pack(side="left")

##------- étiquette de saisie pour la source -------##

label_source = tk.Label(
    cadre_source, text="SOURCE", font=("Arial", 20, "bold")
)
label_source.pack(side="top")

##------- Champ de saisie  pour la source -------##

source = tk.StringVar() # variable texte spéciale de Tkinter
source.set("")    # initialisation de cette variable
saisie_source = tk.Entry(
    cadre_source,
    textvariable=source,
    font=("times", 30),
    bg="#d6eff9",
    width=20,
)
saisie_source.pack(side="top")

##------- Bouton de commande -------##
bouton = tk.Button(
    fenetre,
    text="Codage !",
    command=codage_rot13,  # fonction commandée par le bouton (fonction de rappel)
    font=("Arial", 20, "bold"),
    bg="red",
)
bouton.pack(side="left")

##------- Cadre pour la sortie -------##
cadre_sortie = tk.Frame(fenetre)
cadre_sortie.pack(side="left")


##------- étiquette de sortie  -------##


label_sortie = tk.Label(
    cadre_sortie, text="SORTIE", font=("Arial", 20, "bold")
)
label_sortie.pack(side="top")
affichage_sortie = tk.Label(
    cadre_sortie,
    text="",
    font=("Garamond", 30, "bold"),
    bg="#f9f1d6",
    width=20,
)
affichage_sortie.pack(side="top")

##----- Boucle du gestionnaire d'événements -----##

fenetre.mainloop()
