﻿INTRODUCTION

Il y a un an a peu pres, qu'en faisant a la Bibliotheque royale
des recherches pour mon histoire de Louis XIV, je tombai par
hasard sur les Memoires de M. d'Artagnan, imprimes -- comme la
plus grande partie des ouvrages de cette epoque, ou les auteurs
tenaient a dire la verite sans aller faire un tour plus ou moins
long a la Bastille -- a Amsterdam, chez Pierre Rouge. Le titre me
seduisit: je les emportai chez moi, avec la permission de M. le
conservateur; bien entendu, je les devorai.

Mon intention n'est pas de faire ici une analyse de ce curieux
ouvrage, et je me contenterai d'y renvoyer ceux de mes lecteurs
qui apprecient les tableaux d'epoques. Ils y trouveront des
portraits crayonnes de main de maitre; et, quoique les esquisses
soient, pour la plupart du temps, tracees sur des portes de
caserne et sur des murs de cabaret, ils n'y reconnaitront pas
moins, aussi ressemblantes que dans l'histoire de M. Anquetil, les
images de Louis XIII, d'Anne d'Autriche, de Richelieu, de Mazarin
et de la plupart des courtisans de l'epoque.

Mais, comme on le sait, ce qui frappe l'esprit capricieux du poete
n'est pas toujours ce qui impressionne la masse des lecteurs. Or,
tout en admirant, comme les autres admireront sans doute, les
details que nous avons signales, la chose qui nous preoccupa le
plus est une chose a laquelle bien certainement personne avant
nous n'avait fait la moindre attention.

D'Artagnan raconte qu'a sa premiere visite a M. de Treville, le
capitaine des mousquetaires du roi, il rencontra dans son
antichambre trois jeunes gens servant dans l'illustre corps ou il
sollicitait l'honneur d'etre recu, et ayant nom Athos, Porthos et
Aramis.

Nous l'avouons, ces trois noms etrangers nous frapperent, et il
nous vint aussitot a l'esprit qu'ils n'etaient que des pseudonymes
a l'aide desquels d'Artagnan avait deguise des noms peut-etre
illustres, si toutefois les porteurs de ces noms d'emprunt ne les
avaient pas choisis eux-memes le jour ou, par caprice, par
mecontentement ou par defaut de fortune, ils avaient endosse la
simple casaque de mousquetaire.

Des lors nous n'eumes plus de repos que nous n'eussions retrouve,
dans les ouvrages contemporains, une trace quelconque de ces noms
extraordinaires qui avaient fort eveille notre curiosite.

Le seul catalogue des livres que nous lumes pour arriver a ce but
remplirait un feuilleton tout entier, ce qui serait peut-etre fort
instructif, mais a coups sur peu amusant pour nos lecteurs. Nous
nous contenterons donc de leur dire qu'au moment ou, decourage de
tant d'investigations infructueuses, nous allions abandonner notre
recherche, nous trouvames enfin, guide par les conseils de notre
illustre et savant ami Paulin Paris, un manuscrit in-folio, cote
le n° 4772 ou 4773, nous ne nous le rappelons plus bien, ayant
pour titre:

«Memoires de M. le comte de La Fere, concernant quelques-uns des
evenements qui se passerent en France vers la fin du regne du roi
Louis XIII et le commencement du regne du roi Louis XIV.»

On devine si notre joie fut grande, lorsqu'en feuilletant ce
manuscrit, notre dernier espoir, nous trouvames a la vingtieme
page le nom d'Athos, a la vingt-septieme le nom de Porthos, et a
la trente et unieme le nom d'Aramis.

La decouverte d'un manuscrit completement inconnu, dans une epoque
ou la science historique est poussee a un si haut degre, nous
parut presque miraculeuse. Aussi nous hatames-nous de solliciter
la permission de le faire imprimer, dans le but de nous presenter
un jour avec le bagage des autres a l'Academie des inscriptions et
belles-lettres, si nous n'arrivions, chose fort probable, a entrer
a l'Academie francaise avec notre propre bagage. Cette permission,
nous devons le dire, nous fut gracieusement accordee; ce que nous
consignons ici pour donner un dementi public aux malveillants qui
pretendent que nous vivons sous un gouvernement assez mediocrement
dispose a l'endroit des gens de lettres.

Or, c'est la premiere partie de ce precieux manuscrit que nous
offrons aujourd'hui a nos lecteurs, en lui restituant le titre qui
lui convient, prenant l'engagement, si, comme nous n'en doutons
pas, cette premiere partie obtient le succes qu'elle merite, de
publier incessamment la seconde.

En attendant, comme le parrain est un second pere, nous invitons
le lecteur a s'en prendre a nous, et non au comte de La Fere, de
son plaisir ou de son ennui.

Cela pose, passons a notre histoire.


CHAPITRE PREMIER
LES TROIS PRESENTS DE M. D'ARTAGNAN PERE

Le premier lundi du mois d'avril 1625, le bourg de Meung, ou
naquit l'auteur du Roman de la Rose, semblait etre dans une
revolution aussi entiere que si les huguenots en fussent venus
faire une seconde Rochelle. Plusieurs bourgeois, voyant s'enfuir
les femmes du cote de la Grande-Rue, entendant les enfants crier
sur le seuil des portes, se hataient d'endosser la cuirasse et,
appuyant leur contenance quelque peu incertaine d'un mousquet ou
