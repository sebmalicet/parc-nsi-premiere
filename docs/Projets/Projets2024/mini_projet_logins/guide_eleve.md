---
title:  Mini Projet guidé : logins réseaux
layout: parc
---


# Problème à résoudre

> __Règle 1 :__  Il faut d'abord bien comprendre le problème, ses entrées et les  sorties attendues.

Vous disposez  d'un fichier [prenom_nom.csv](https://fr.wikipedia.org/wiki/Comma-separated_values) de mille lignes ou plus, listant les utilisateurs d'un réseau informatique, dont chaque ligne est  au format `numero, nom, prenom` :

~~~python
1,Timothée,Gaillard
2,Zacharie,Lefort
3,Joseph,Lambert
~~~

Vous devez générer pour chaque utilisateur un __login__ `prenom.nom` en minuscules, sans accents, ni cédilles et un __mot de passe__ aléatoire respectant la politique suivante :

* constitué de dix symboles choisis parmi les 4 classes suivantes : caractères alphabétiques minuscules, caractères majuscules, chiffres  ou l'un des caractères spéciaux '?!$#'.
* au moins un caractère de chaque classe

Ces identifiants de connexion devront être stockés (en clair certes ...) dans un fichier [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values) dont chaque ligne sera  au format `numero, nom, prenom,login,mdp` :

~~~python
1,Timothée,Gaillard,timothee.gaillard,AToKlBso4M
2,Zacharie,Lefort,zacharie.lefort,4E7hFZgXNt
3,Joseph,Lambert,joseph.lambert,G6PtmimT0J
~~~


# Cahier des charges

> __Règle 2  :__  On commence par réfléchir avec papier et crayon pour déterminer les étapes à réaliser et isoler les tâches élémentaires dans des fonctions.


## Question 1

Pour déterminer les étapes  du __cahier des charges__, complétez la cart heuristique en lien : <https://ladigitale.dev/digimindmap/#/m/64f60ee50975c> 

En *programmation modulaire*, on représente chaque tâche élémentaire par une fonction. Déterminez une  liste de fonctions qu'on pourrait écrire pour répondre  au __cahier des charges__.


## Question 2

Précisez pour chaque fonction ses _paramètres_ avec leurs types et les valeurs renvoyées avec leurs types. Une fonction peut ne prendre aucun paramètre en entrée et ne renvoyer aucune valeur en sortie.

Un exemple :

~~~python
def generer_login(prenom, nom):
    """
    Génère un login de type prenom.nom
    à partir des valeurs de  prenom et de nom convertis en minuscules
    """
~~~

Vous travaillerez en binôme avec le pad collaboratif [Digipad](https://ladigitale.dev/blog/digipad-des-murs-multimedias-pour-vos-activites-collaboratives)

# Algorithmes et structures de données

> __Règle 3  :__  Une fois qu'on a déterminé ce qu'on souhaiterait réaliser, on se pose la question du comment : de quelle façon (algorithmes) et avec  quels outils : structures ou styles de programmation (boucles, conditions, programmation objet ou fonctionnelle) et structures de données (entier, chaîne de caractères, tableau, dictionnaire, arbre ...)


## Question 3


Dans ce projet, de quels types d'algorithmes pourriez-vous avoir besoin ?

* _balayage séquentiel_ : parcourir une structure de données élément par élément
* _recherche dichotomique_ : diviser un ensemble ordonné en deux et réduire la recherche à la première ou la seconde moitié comme dans le _Juste prix_
* _de tri_ : ordonner des information selon un certain critère


## Question 4


Parmi les types de données Python suivants, quels sont ceux qui pourraient être utiles dans ce projet ? Si oui pour quelle(s) fonction(s) ?

|Type|Nom Python|Exemple de valeur|
|:---:|:---:|:---:|
|entier|`int`|`4`|
|flottant|`float`|`3.14`|
|booléen|`bool`|`True` ou `False`|
|chaîne de caractères|`str`|`'edgar'`|
|tableau/liste|`list`|`['8', 'edgar', 'quinet']`|
|dictionnaire|`dict`|`{'nom': 'quinet', 'prenom': 'edgar'}`|

## Question 5 

Dans cette question on explore quelques types de bases en Python utiles pour ce projet.


### Question 5.a

Dans ce projet vous aurez besoin de manipuler des séquences ordonnées d'éléments repérés par leur index dans la séquence. 
En Python il s'agit de tableaux dynamiques (on peut modifier leur taille) du type `list`. Voici quelques exemples de manipulation de tableaux dynamiques :

~~~python
>>> tab = [] # tableau vide
>>> tab.append(5)  # ajout d'un élément à la fin
>>> tab.append(6)
>>> tab
[5, 6]
>>> tab[0]  # les éléments sont repérés par leur index qui commence à 0
5
>>> tab[1]
6
>>> len(tab) # longueur du tableau
2
>>> tab[1] = 7
>>> tab
7
>>> for k in range(len(tab)): # parcours par index
        print(k, tab[k])
(0, 5)
(1, 6)
>>> for e in tab:  # parcours par élément
        print(e)
5
6
>>> t2 = [k ** 2 for k in range(1, 5)] # tableau généré en compréhension
>>> t2
[1, 4, 9, 16]
~~~

Donnez deux façons de construire un tableau contenant les cubes de tous les entiers successifs entre 15 et 20.


### Question 5.b


Dans ce projet vous aurez besoin de générer des chaînes de caractères de type `str` en Python. Voici quelques exemples de manipulations de chaînes de caractères :

~~~python
>>> chaine = 'amour'
>>> chaine[0]
'a'
>>> chaine[2]
'o'
>>> for c in chaine:
        print(c)
a
m
o
u
r
>>> chaine2 = chaine + 'rette'
>>> chaine2
'amourette'
>>> len(chaine2)
9
~~~

Écrire une fonction  qui prend en paramètre une chaîne de caractères et qui renvoie la chaîne lue à l'envers.

### Question 5.c

Dans ce projet vous aurez besoin de générer des entiers aléatoires. Dans un [IDE](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement) Python, importez le module `random` puis affichez la documentation de la fonction `random.randint` avec `help(random.randint)`  dans la console.

Donnez une expression permettant de construire un tableau de dix entiers aléatoires entre 1 et 6.


### Questions 5.d

Dans ce projet vous aurez besoin d'accéder au contenu de fichiers textes en lecture ou en écriture.

Pour lire ligne par ligne un fichier 'fichier.txt' : 

~~~python
reader = open('fichier.txt', mode='r') # ouverture
for ligne in reader:
    # traitement sur la ligne
reader.close() # fermeture
~~~

Pour écrire dans un fichier les chaînes de caractères contenues dans un tableau `tab`:

~~~python
writer= open('fichier.txt', mode='w') # ouverture
for element in tab:
    writer.write(element + '\n') # '\n' est le saut de ligne
writer.close() # fermeture
~~~

Comme exercice, ouvrez le fichier `prenom_nom.csv`  fourni dans [le matériel](materiel.zip)  et recopiez ligne à ligne son contenu dans `copie_prenom_nom.csv`.

# Développement et écriture du code

>  __Règle 4  :__  Après avoir découpé la réalisation du projet en fonctions et choisi pour chacune un ou des types de structures de données et un algorithme pour les traiter, il reste à les implémenter dans le langage de programmation choisi, Python pour vous. Si on sait prédire la sortie que doit générer une fonction pour un certaine entrée, on peut construire un **jeu de tests** afin de vérifier qu'elle fonctionne comme attendu sur quelques cas bien choisis. Cela ne prouvera cependant pas qu'elle est correcte.


## Question 6


Récupérez le lien avec l'archive contenant [le matériel](materiel.zip) puis complétez les fonctions définies dans le **cahier des charges**. Le squelette de  code  contient au moins une fonction de test.  

## Question 7

En pratique, on a besoin de construire des fichiers d'identifiants de connexion régulièrement. Il serait donc d'étendre notre programme avec une [Interface Homme Machine](https://fr.wikipedia.org/wiki/Interactions_homme-machine). Dans le squelette de code, complétez la fonction d''interface textuelle puis celle d'interface graphique. Pour cette dernière vous utiliserez le module `nsi_ui` fourni dans [le matériel](materiel.zip). Une documentation est disponible page 52 du [manuel Déclic de Première NSI](https://mesmanuels.fr/acces-libre/3813624).


