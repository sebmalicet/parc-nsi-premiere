---
title:  Chapitre 18,  dictionnaires
layout: parc
---




Cours de Frédéric Junier.


## TP 

* [TP Scrabble (introduction ludique)](chapitre18/scrabble.md)
* [TP dictionnaires](chapitre18/TP/TP-Dictionnaires-1.pdf)

## Cours 

* [Cours version pdf](chapitre18/Cours/dictionnaires-cours-.pdf)
* [Cours version markdown](chapitre18/Cours/dictionnaires-cours-git.md)
* [Exemples et exercices du cours, carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180)

## Cartes mentales 

* [Carte mentale sur les dictionnaires au format HTML](./chapitre18/Cours/Cartes_mentales/carte_mentale_dictionnaires.html)

* [Carte mentale sur les dictionnaires au format pdf](./chapitre18/Cours/Cartes_mentales/carte_mentale_dictionnaires.pdf)
  
## Videos

??? video

    * Présentation par David Latouche

    <iframe width="948" height="533" src="https://www.youtube.com/embed/ON_Ac-MbHik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    * [Video Lumni de Charles Poulmaire](https://www.lumni.fr/video/les-dictionnaires)