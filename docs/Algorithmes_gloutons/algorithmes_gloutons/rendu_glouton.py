def rendu_glouton(restant, pieces):
    # pieces tableau de valeurs de pièces disponibles dans l'ordre croissant
    indice_pieces = len(pieces) - 1
    rendu = []
    while ... and  ...:
        if pieces[...] <= restant:
            restant = ...
            rendu.append(...)
        else:
            indice_pieces = ...
    # rendu possible
    return rendu

def test_rendu_glouton():
    systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]
    assert rendu_glouton(76, systeme_euro) == [50, 20, 5, 1]
    assert rendu_glouton(49, systeme_euro) == [20, 20, 5, 2, 2]
    assert rendu_glouton(843, systeme_euro) == [500, 200, 100, 20, 20, 2, 1]
    systeme_non_canonique = [1, 3, 6, 12, 24, 30]
    assert rendu_glouton(49 , systeme_non_canonique) == [30, 12, 6, 1]
    assert rendu_glouton(53 , systeme_non_canonique) == [30, 12, 6, 3, 1, 1]
    print("tests réussis pour rendu_glouton")